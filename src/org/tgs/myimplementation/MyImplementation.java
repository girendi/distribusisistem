/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.myimplementation;

import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import org.tgs.mydao.Account;
import org.tgs.mydao.Announcement;
import org.tgs.mydao.Comments;
import org.tgs.mydata.MyData;
import org.tgs.myinterface.InstallInterface;

/**
 *
 * @author Gideon Panjaitan
 */
public class MyImplementation extends UnicastRemoteObject implements InstallInterface {

    public MyImplementation() throws RemoteException {
        
    }

    @Override
    public ArrayList findAllAccount() throws RemoteException {
        try {
            System.out.println("Invoke findAllAccount from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.findAllAccount();
    }

    @Override
    public List Login(String nim, String pass) throws RemoteException {
        try {
            System.out.println("Invoke login from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.Login(nim, pass);
    }

    @Override
    public int TambahAccount(Account a) throws RemoteException {
        try {
            System.out.println("Invoke Save from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.tambahAccount(a);
    }

    @Override
    public int updateAccount(Account a, String nim) throws RemoteException {
        try {
            System.out.println("Invoke Update from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.updateAccount(a, nim);
    }

    @Override
    public int deleteAccount(String nim) throws RemoteException {
        try {
            System.out.println("Invoke Delete from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.deleteAccount(nim);
    }

    @Override
    public ArrayList findAllAnnouncement() throws RemoteException {
        try {
            System.out.println("Invoke findAllAnnouncement from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.findAllAnnouncement();
    }

    @Override
    public int TambahAnnouncement(Announcement an) throws RemoteException {
        try {
            System.out.println("Invoke TambahAnnouncement from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.TambahAnnouncement(an);
    }

    @Override
    public int TambahComments(Comments cm) throws RemoteException {
        try {
            System.out.println("Invoke TambahComment from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.TambahComments(cm);
    }

    @Override
    public ArrayList findAllComment() throws RemoteException {
        try {
            System.out.println("Invoke findAllComments from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.findAllComment();
    }

    @Override
    public ArrayList findComment(String idcom) throws RemoteException {
        try {
            System.out.println("Invoke findComments from " + getClientHost());
        } catch (ServerNotActiveException snae) {
            snae.printStackTrace();
        }
        return MyData.findComment(idcom);
    }
}
