/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.mydao;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 *
 * @author Gideon Panjaitan
 */
public class Account implements Serializable{
    private int id;
    private String name;
    private String nim;
    private String password;
    private String prodi;
    private String angkatan;
    private String role;

    public Account() throws RemoteException{
        
    }

    public Account(int id, String name, String nim, String password, String prodi, String angkatan, String role) throws RemoteException{
        this.id = id;
        this.name = name;
        this.nim = nim;
        this.password = password;
        this.prodi = prodi;
        this.angkatan = angkatan;
        this.role = role;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProdi() {
        return prodi;
    }

    public void setProdi(String prodi) {
        this.prodi = prodi;
    }

    public String getAngkatan() {
        return angkatan;
    }

    public void setAngkatan(String angkatan) {
        this.angkatan = angkatan;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    
}
