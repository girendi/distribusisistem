/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.mydao;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 *
 * @author Gideon Panjaitan
 */
public class Announcement implements Serializable{
    private int id;
    private String nama;
    private String judul;
    private String isi;

    public Announcement() throws RemoteException{
    }

    public Announcement(int id, String nama, String judul, String isi) {
        this.id = id;
        this.nama = nama;
        this.judul = judul;
        this.isi = isi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }   
    
}
