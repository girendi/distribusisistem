/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.mydao;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 *
 * @author Gideon Panjaitan
 */
public class Comments implements Serializable{
    private int id;
    private String id_announcement;
    private String nama;
    private String comments;

    public Comments() throws RemoteException{
    }

    public Comments(int id, String id_announcement, String nama, String comments) {
        this.id = id;
        this.id_announcement = id_announcement;
        this.nama = nama;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_announcement() {
        return id_announcement;
    }

    public void setId_announcement(String id_announcement) {
        this.id_announcement = id_announcement;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
