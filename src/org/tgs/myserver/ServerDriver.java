/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.myserver;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import org.tgs.myimplementation.MyImplementation;

/**
 *
 * @author Gideon Panjaitan
 */
public class ServerDriver {

    public static void main(String[] args) throws RemoteException {
        try{
            Registry reg = LocateRegistry.createRegistry(1099);
            MyImplementation mi = new MyImplementation();
            reg.rebind("server", mi);
            System.out.println("Server is ready....");
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
