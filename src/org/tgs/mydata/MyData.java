/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.mydata;

import com.mysql.jdbc.Connection;
import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.tgs.mycon.MyConnection;
import org.tgs.mydao.Account;
import org.tgs.mydao.Announcement;
import org.tgs.mydao.Comments;

/**
 *
 * @author Gideon Panjaitan
 */
public class MyData {

    private MyConnection db = new MyConnection();

    public static ArrayList findAllAccount() throws RemoteException {
        ArrayList arr = new ArrayList();
        try {
            String QRY = "SELECT * FROM Account ORDER BY Id";
            Connection con = MyConnection.getInstance().getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(QRY);

            while (rs.next()) {
                Account a = new Account();
                a.setId(rs.getInt("id"));
                a.setName(rs.getString("nama"));
                a.setNim(rs.getString("nim"));
                a.setPassword(rs.getString("password"));
                a.setProdi(rs.getString("prodi"));
                a.setAngkatan(rs.getString("angkatan"));
                a.setRole(rs.getString("role"));
                arr.add(a);
            }

            stmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }
        return arr;
    }

    public static int updateAccount(Account a, String nim) {
        int iRet = -1;
        try {
            Connection con = MyConnection.getInstance().getConnection();
            String SQL = "UPDATE account SET nama=?, nim=?, password=?, prodi=?, angkatan=?, role=? WHERE nim= '" + nim + "'";
            PreparedStatement pstmt = con.prepareStatement(SQL);

            pstmt.setString(1, a.getName());
            pstmt.setString(2, a.getNim());
            pstmt.setString(3, a.getPassword());
            pstmt.setString(4, a.getProdi());
            pstmt.setString(5, a.getAngkatan());
            pstmt.setString(6, a.getRole());

            iRet = pstmt.executeUpdate();

            pstmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }

        return iRet;
    }

    public static List Login(String nim, String pass) throws RemoteException {
        List<Account> data = new ArrayList<>();
        try {
            String QRY = "SELECT * FROM account WHERE nim='" + nim + "' AND password='" + pass + "'";
            Connection con = MyConnection.getInstance().getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(QRY);

            while (rs.next()) {
                Account ac = new Account();
                ac.setId(rs.getInt("id"));
                ac.setName(rs.getString("nama"));
                ac.setNim(rs.getString("nim"));
                ac.setPassword(rs.getString("password"));
                ac.setProdi(rs.getString("prodi"));
                ac.setAngkatan(rs.getString("angkatan"));
                ac.setRole(rs.getString("role"));
                data.add(ac);
            }
            stmt.close();
        } catch (Exception x) {
            JOptionPane.showMessageDialog(null, "Login gagal, Pesan Error : \n" + x);
        }
        return data;
    }

    public static int tambahAccount(Account a) {
        int iRet = -1;
        try {
            Connection con = MyConnection.getInstance().getConnection();
            String SQL = "INSERT INTO account(nama, nim, password, prodi, angkatan, role) Values(?,?,?,?,?,?)";
            PreparedStatement pstmt = con.prepareStatement(SQL);

            pstmt.setString(1, a.getName());
            pstmt.setString(2, a.getNim());
            pstmt.setString(3, a.getPassword());
            pstmt.setString(4, a.getProdi());
            pstmt.setString(5, a.getAngkatan());
            pstmt.setString(6, a.getRole());

            iRet = pstmt.executeUpdate();

            pstmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }

        return iRet;
    }

    public static int deleteAccount(String nim) {
        int iRet = -1;
        try {
            Connection con = MyConnection.getInstance().getConnection();
            String SQL = "Delete from account where nim = '" + nim + "'";
            PreparedStatement pstmt = con.prepareStatement(SQL);
            iRet = pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(MyData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return iRet;
    }
    
    public static ArrayList findAllAnnouncement() throws RemoteException {
        ArrayList arr = new ArrayList();
        try {
            String QRY = "SELECT * FROM announcement ORDER BY Id desc";
            Connection con = MyConnection.getInstance().getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(QRY);

            while (rs.next()) {
                Announcement an = new Announcement();
                an.setId(rs.getInt("id"));
                an.setNama(rs.getString("nama"));
                an.setJudul(rs.getString("judul"));
                an.setIsi(rs.getString("isi"));
                arr.add(an);
            }

            stmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }
        return arr;
    }
    
    public static int TambahAnnouncement(Announcement a) {
        int iRet = -1;
        try {
            Connection con = MyConnection.getInstance().getConnection();
            String SQL = "INSERT INTO announcement(nama, judul, isi) Values(?,?,?)";
            PreparedStatement pstmt = con.prepareStatement(SQL);

            pstmt.setString(1, a.getNama());
            pstmt.setString(2, a.getJudul());
            pstmt.setString(3, a.getIsi());

            iRet = pstmt.executeUpdate();

            pstmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }

        return iRet;
    }
    
    public static int TambahComments(Comments cm) {
        int iRet = -1;
        try {
            Connection con = MyConnection.getInstance().getConnection();
            String SQL = "INSERT INTO comments (id_announcement, nama, comment) Values(?,?,?)";
            PreparedStatement pstmt = con.prepareStatement(SQL);

            pstmt.setString(1, cm.getId_announcement());
            pstmt.setString(2, cm.getNama());
            pstmt.setString(3, cm.getComments());

            iRet = pstmt.executeUpdate();

            pstmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }

        return iRet;
    }
    
    public static ArrayList findAllComment() throws RemoteException {
        ArrayList arr = new ArrayList();
        try {
            String QRY = "SELECT * FROM comments ORDER BY Id desc";
            Connection con = MyConnection.getInstance().getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(QRY);

            while (rs.next()) {
                Comments cm = new Comments();
                cm.setId(rs.getInt("id"));
                cm.setId_announcement(rs.getString("id_announcement"));
                cm.setNama(rs.getString("nama"));
                cm.setComments(rs.getString("comment"));
                
                arr.add(cm);
            }

            stmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }
        return arr;
    }
    public static ArrayList findComment(String idcom) throws RemoteException {
        ArrayList arr = new ArrayList();
        try {
            String QRY = "SELECT * FROM comments where id_announcement = '" + idcom + "' ORDER BY Id desc";
            Connection con = MyConnection.getInstance().getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(QRY);

            while (rs.next()) {
                Comments cm = new Comments();
                cm.setId(rs.getInt("id"));
                cm.setId_announcement(rs.getString("id_announcement"));
                cm.setNama(rs.getString("nama"));
                cm.setComments(rs.getString("comment"));
                
                arr.add(cm);
            }

            stmt.close();
        } catch (SQLException se) {
            System.out.println(se);
        }
        return arr;
    }
}
