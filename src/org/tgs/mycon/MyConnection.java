/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.mycon;

import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Gideon Panjaitan
 */
public final class MyConnection {

    private static MyConnection _instance = null;
    private com.mysql.jdbc.Connection _con = null;

    public MyConnection() {
        //Connect to Ms Access
        _con = getSQLServerConnection();
    }

    public static synchronized MyConnection getInstance() {
        if (_instance == null) {
            _instance = new MyConnection();
        }
        return _instance;
    }

    public com.mysql.jdbc.Connection getConnection() {
        return _con;
    }
    
    private static com.mysql.jdbc.Connection getSQLServerConnection() {
    com.mysql.jdbc.Connection con = null;
 
    try {
      Class.forName("com.mysql.jdbc.Driver");
 
      try {
                String url, user, password;
                url = "jdbc:mysql://localhost:3306/pengumuman_db";
                user = "root";
                password = "";
                con = (com.mysql.jdbc.Connection) DriverManager.getConnection(url, user, password);
                System.out.println("Koneksi Sukses");
            } catch (SQLException se) {
                JOptionPane.showMessageDialog(null, "Koneksi Gagal!" + se);
                System.exit(0);
            }
    } catch (Exception e) {
      System.out.println(e);
    }
    return con;
  }
}
