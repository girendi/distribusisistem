/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.myclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.util.Util;
import org.tgs.mydao.Account;
import org.tgs.myinterface.InstallInterface;

/**
 *
 * @author Gideon Panjaitan
 */
public class AdminLayoutMaster extends javax.swing.JFrame {

    AdminLayoutMaster.MessageReceiver msr = new AdminLayoutMaster.MessageReceiver();
    
    private DefaultTableModel tabelModel = null;

    /**
     * Creates new form AdminLayoutMaster
     */
    public AdminLayoutMaster() {
        initComponents();
        buatTabel();
        loadTable();
        bersihText();
        lblId.setVisible(false);
        txtId.setVisible(false);
        btnTambah.setEnabled(true);
        btnEdit.setEnabled(false);
        btnHapus.setEnabled(false);
        btnt.setEnabled(false);
        try {
            msr.start();
        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    private void buatTabel() {
        tabelModel = new DefaultTableModel();
        tabelModel.addColumn("Nama");
        tabelModel.addColumn("Nim");
        tabelModel.addColumn("Password");
        tabelModel.addColumn("Prodi");
        tabelModel.addColumn("Angkatan");
        tabelModel.addColumn("Role");
        tblAccount.setModel(tabelModel);
    }

    private void bersihText() {
        txtId.setText(null);
        txtNama.setText(null);
        txtNim.setText(null);
        txtPassword.setText(null);
        cmbRoll.setSelectedItem(null);
        cmbAngkatan.setSelectedItem(null);
        cmbProdi.setSelectedItem(null);
    }

    private void loadTable() {
        try {
            tabelModel.getDataVector().removeAllElements();
            tabelModel.fireTableDataChanged();
            List<Account> data = new ArrayList<>();
            Registry req = LocateRegistry.getRegistry("localhost", 1099);
            InstallInterface ii = (InstallInterface) req.lookup("server");
            data = ii.findAllAccount();
            for (int x = 0; x < data.size(); x++) {
                Object[] ob = new Object[6];
                
                ob[0] = data.get(x).getName();
                ob[1] = data.get(x).getNim();
                ob[2] = data.get(x).getPassword();
                ob[3] = data.get(x).getProdi();
                ob[4] = data.get(x).getAngkatan();
                ob[5] = data.get(x).getRole();
                tabelModel.addRow(ob);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    public class MessageReceiver extends ReceiverAdapter {
        JChannel channel;
        final List<String> state = new LinkedList<String>();
        final List<View> memberList = new LinkedList<View>();
        
        @Override
        public void viewAccepted(View new_view) {
            System.out.println("** view: " + new_view);
            memberList.add(new_view);
        }
        
        @Override
        public void receive(Message msg) {
            String line = msg.getSrc() + ": " + msg.getObject();
            System.out.println(line);
            //loadTable();

            synchronized (state) {
                state.add(line);
            }
        }
        
        @Override
        public void getState(OutputStream output) throws Exception {
            synchronized (state) {
                Util.objectToStream(state, new DataOutputStream(output));
            }
        }
        
        @Override
        public void setState(InputStream input) throws Exception {
            List<String> list = (List<String>) Util.objectFromStream(new DataInputStream(input));
            synchronized (state) {
                state.clear();
                state.addAll(list);
            }
            System.out.println("received state (" + list.size() + " messages in chat history):");

            for (String str : list) {
                System.out.println(str);
            }
        }
        
        private void start() throws Exception {
            channel = new JChannel();
            channel.setReceiver(this);
            channel.connect("server");
            channel.getState(null, 1000);
        }
        
        public void publishUpdate() {
            String line = "Data berhasil diubah";
            Message msg = new Message(null, line);
            try {
                channel.send(msg);
                //log_pertandingan.setText("Data ditambahkan");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        public void publishCreate() {
            String line = "Data baru dimasukkan";
            Message msg = new Message(null, line);
            try {
                channel.send(msg);
                //log_pertandingan.setText("Data ditambahkan");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        public void publishDelete() {
            String line = "Data telah dihapus";
            Message msg = new Message(null, line);
            try {
                channel.send(msg);
                //log_pertandingan.setText("Data ditambahkan");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblAccount = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        lblId = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        txtNama = new javax.swing.JTextField();
        txtNim = new javax.swing.JTextField();
        txtPassword = new javax.swing.JPasswordField();
        cmbRoll = new javax.swing.JComboBox<>();
        btnTambah = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        cmbAngkatan = new javax.swing.JComboBox<>();
        cmbProdi = new javax.swing.JComboBox<>();
        btnt = new javax.swing.JButton();
        btnPengumuman = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 24)); // NOI18N
        jLabel1.setText("Selamat Datang Admin");

        tblAccount.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        tblAccount.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Nama", "Nim", "Password", "Prodi", "Angkatan", "Roll"
            }
        ));
        tblAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblAccountMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblAccount);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("CRUD Information"));

        lblId.setText("ID");

        jLabel3.setText("Nama");

        jLabel4.setText("Nim");

        jLabel5.setText("Password");

        jLabel6.setText("Prodi");

        jLabel7.setText("Angkatan");

        jLabel8.setText("Roll");

        txtId.setEditable(false);

        cmbRoll.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Admin", "User" }));

        btnTambah.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnTambah.setText("Tambah");
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnEdit.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnEdit.setText("Ubah");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnHapus.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        btnHapus.setText("Hapus");
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        cmbAngkatan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020" }));

        cmbProdi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "D3TK", "D3TI", "D4TI", "S1TI", "S1SI", "S1TE", "S1MR", "S1BP" }));

        btnt.setText("+");
        btnt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblId)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtId)
                            .addComponent(txtNama)
                            .addComponent(txtNim)
                            .addComponent(txtPassword)
                            .addComponent(cmbRoll, 0, 150, Short.MAX_VALUE)
                            .addComponent(cmbAngkatan, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbProdi, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnTambah)
                        .addGap(30, 30, 30)
                        .addComponent(btnEdit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                        .addComponent(btnHapus))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnt)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblId)
                    .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtNim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(cmbProdi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cmbAngkatan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(cmbRoll, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTambah)
                    .addComponent(btnEdit)
                    .addComponent(btnHapus))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
                .addComponent(btnt))
        );

        btnPengumuman.setText("Lihat Pengumuman");
        btnPengumuman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPengumumanActionPerformed(evt);
            }
        });

        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnPengumuman, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLogout))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
            .addGroup(layout.createSequentialGroup()
                .addGap(217, 217, 217)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnPengumuman)
                            .addComponent(btnLogout)))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
        if (txtNama.getText().isEmpty() || txtNim.getText().isEmpty() || txtPassword.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Lengkapi inputan account");
        } else {
            try {
                Account ac = new Account();
                ac.setName(txtNama.getText());
                ac.setNim(txtNim.getText());
                ac.setPassword(txtPassword.getText());
                ac.setProdi(cmbProdi.getSelectedItem().toString());
                ac.setAngkatan(cmbAngkatan.getSelectedItem().toString());
                ac.setRole(cmbRoll.getSelectedItem().toString());

                Registry req = LocateRegistry.getRegistry("localhost", 1099);
                InstallInterface ii = (InstallInterface) req.lookup("server");

                ii.TambahAccount(ac);
                loadTable();
                bersihText();
                btnTambah.setEnabled(true);
                btnEdit.setEnabled(false);
                btnHapus.setEnabled(false);
                msr.publishCreate();
            } catch (RemoteException ex) {
                Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotBoundException ex) {
                Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        if (txtNama.getText().isEmpty() || txtNim.getText().isEmpty() || txtPassword.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Lengkapi inputan account");
        } else {
            try {
                Account ac = new Account();

                ac.setName(txtNama.getText());
                ac.setNim(txtNim.getText());
                ac.setPassword(txtPassword.getText());
                ac.setProdi(cmbProdi.getSelectedItem().toString());
                ac.setAngkatan(cmbAngkatan.getSelectedItem().toString());
                ac.setRole(cmbRoll.getSelectedItem().toString());

                Registry req = LocateRegistry.getRegistry("localhost", 1099);
                InstallInterface ii = (InstallInterface) req.lookup("server");

                String nim = ac.getNim();

                ii.updateAccount(ac, nim);

                loadTable();
                bersihText();
                btnTambah.setEnabled(true);
                btnEdit.setEnabled(false);
                btnHapus.setEnabled(false);
                msr.publishUpdate();
            } catch (RemoteException ex) {
                Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotBoundException ex) {
                Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void tblAccountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblAccountMouseClicked
        // TODO add your handling code here:
        int baris = tblAccount.getSelectedRow();
        txtNama.setText(tblAccount.getModel().getValueAt(baris, 0).toString());
        txtNim.setText(tblAccount.getModel().getValueAt(baris, 1).toString());
        txtPassword.setText(tblAccount.getModel().getValueAt(baris, 2).toString());
        cmbProdi.setSelectedItem(tblAccount.getModel().getValueAt(baris, 3).toString());
        cmbAngkatan.setSelectedItem(tblAccount.getModel().getValueAt(baris, 4).toString());
        cmbRoll.setSelectedItem(tblAccount.getModel().getValueAt(baris, 5).toString());
        txtNim.setEnabled(false);
        cmbProdi.setEnabled(false);
        cmbAngkatan.setEnabled(false);
        btnTambah.setEnabled(false);
        btnEdit.setEnabled(true);
        btnHapus.setEnabled(true);
        btnt.setEnabled(true);
    }//GEN-LAST:event_tblAccountMouseClicked

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        try {
            // TODO add your handling code here:
            int baris = tblAccount.getSelectedRow();
            Account ac = new Account();
            Registry req = LocateRegistry.getRegistry("localhost", 1099);
            InstallInterface ii = (InstallInterface) req.lookup("server");
            
            ac.setNim(tblAccount.getModel().getValueAt(baris, 1).toString());
            ii.deleteAccount(ac.getNim());
            loadTable();
            bersihText();
            btnTambah.setEnabled(true);
            btnEdit.setEnabled(false);
            btnHapus.setEnabled(false);
            msr.publishDelete();
        } catch (RemoteException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnHapusActionPerformed

    private void btnPengumumanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPengumumanActionPerformed
        // TODO add your handling code here:
        UserLayoutMaster Ulm = new UserLayoutMaster();
        Ulm.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnPengumumanActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        Login lg = new Login();
        lg.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btntActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntActionPerformed
        // TODO add your handling code here:
        bersihText();
        btnt.setEnabled(false);
        btnTambah.setEnabled(true);
        btnEdit.setEnabled(false);
        btnHapus.setEnabled(false);
        txtNim.setEnabled(true);
        cmbProdi.setEnabled(true);
        cmbAngkatan.setEnabled(true);
    }//GEN-LAST:event_btntActionPerformed

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnPengumuman;
    private javax.swing.JButton btnTambah;
    private javax.swing.JButton btnt;
    private javax.swing.JComboBox<String> cmbAngkatan;
    private javax.swing.JComboBox<String> cmbProdi;
    private javax.swing.JComboBox<String> cmbRoll;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblId;
    private javax.swing.JTable tblAccount;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtNama;
    private javax.swing.JTextField txtNim;
    private javax.swing.JPasswordField txtPassword;
    // End of variables declaration//GEN-END:variables
}
