/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.myclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.util.Util;
import org.tgs.mydao.Announcement;
import org.tgs.mydao.Comments;
import org.tgs.myinterface.InstallInterface;

/**
 *
 * @author Gideon Panjaitan
 */
public class UserLayoutMaster extends javax.swing.JFrame {
    private DefaultTableModel tabelModel = null;
    private DefaultTableModel tabelModel1 = null;
    UserLayoutMaster.MessageReceiver msr = new UserLayoutMaster.MessageReceiver();
    /**
     * Creates new form UserLayoutMaster
     */
    public UserLayoutMaster() {
        initComponents();
        buatTabel1();
        buatTabel2();
        loadTable();
        loadTable2();
        bersihText();
        btnTambah.setVisible(false);
        try {
            msr.start();
        } catch (Exception ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public class MessageReceiver extends ReceiverAdapter {
        JChannel channel;
        final List<String> state = new LinkedList<String>();
        final List<View> memberList = new LinkedList<View>();
        
        @Override
        public void viewAccepted(View new_view) {
            System.out.println("** view: " + new_view);
            memberList.add(new_view);
        }
        
        @Override
        public void receive(Message msg) {
            String line = msg.getSrc() + ": " + msg.getObject();
            System.out.println(line);
            //loadTable();

            synchronized (state) {
                state.add(line);
            }
        }
        
        @Override
        public void getState(OutputStream output) throws Exception {
            synchronized (state) {
                Util.objectToStream(state, new DataOutputStream(output));
            }
        }
        
        @Override
        public void setState(InputStream input) throws Exception {
            List<String> list = (List<String>) Util.objectFromStream(new DataInputStream(input));
            synchronized (state) {
                state.clear();
                state.addAll(list);
            }
            System.out.println("received state (" + list.size() + " messages in chat history):");

            for (String str : list) {
                System.out.println(str);
            }
        }
        
        private void start() throws Exception {
            channel = new JChannel();
            channel.setReceiver(this);
            channel.connect("server");
            channel.getState(null, 1000);
        }
        
        public void tambahTanggapan() {
            String line = "Berhasil Memberikan Tanggapan";
            Message msg = new Message(null, line);
            try {
                channel.send(msg);
                //log_pertandingan.setText("Data ditambahkan");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
    }
    
    private void bersihText() {
        txtPP.setText(null);
        txtJudul.setText(null);
        txtIsi.setText(null);
        txtIdPen.setText(null);
        txtNamaR.setText(null);
        txtKomentar.setText(null);
    }
    
    private void buatTabel1() {
        tabelModel = new DefaultTableModel();
        tabelModel.addColumn("Id");
        tabelModel.addColumn("Nama");
        tabelModel.addColumn("Judul");
        tabelModel.addColumn("Isi");
        tblPengumuman.setModel(tabelModel);
    }
    
    private void buatTabel2() {
        tabelModel1 = new DefaultTableModel();
        
        tabelModel1.addColumn("Id Pengumuman");
        tabelModel1.addColumn("Nama Responden");
        tabelModel1.addColumn("Komentar");
        tblRespon.setModel(tabelModel1);
    }
    
    private void loadTable() {
        try {
            tabelModel.getDataVector().removeAllElements();
            tabelModel.fireTableDataChanged();
            List<Announcement> data = new ArrayList<>();
            Registry req = LocateRegistry.getRegistry("localhost", 1099);
            InstallInterface ii = (InstallInterface) req.lookup("server");
            data = ii.findAllAnnouncement();
            for (int x = 0; x < data.size(); x++) {
                Object[] ob = new Object[4];
                ob[0] = data.get(x).getId();
                ob[1] = data.get(x).getNama();
                ob[2] = data.get(x).getJudul();
                ob[3] = data.get(x).getIsi();
                tabelModel.addRow(ob);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadTable2() {
        try {
            tabelModel1.getDataVector().removeAllElements();
            tabelModel1.fireTableDataChanged();
            List<Comments> data = new ArrayList<>();
            Registry req = LocateRegistry.getRegistry("localhost", 1099);
            InstallInterface ii = (InstallInterface) req.lookup("server");
            data = ii.findAllComment();
            for (int x = 0; x < data.size(); x++) {
                Object[] ob = new Object[3];
                ob[0] = data.get(x).getId_announcement();
                ob[1] = data.get(x).getNama();
                ob[2] = data.get(x).getComments();
                tabelModel1.addRow(ob);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadTable3() {
        try {
            tabelModel1.getDataVector().removeAllElements();
            tabelModel1.fireTableDataChanged();
            List<Comments> data = new ArrayList<>();
            Comments cm = new Comments();
            Registry req = LocateRegistry.getRegistry("localhost", 1099);
            InstallInterface ii = (InstallInterface) req.lookup("server");
            int baris = tblPengumuman.getSelectedRow();
            cm.setId_announcement(tblPengumuman.getModel().getValueAt(baris, 1).toString());
            data = ii.findComment(cm.getId_announcement());
            for (int x = 0; x < data.size(); x++) {
                Object[] ob = new Object[4];
                ob[0] = data.get(x).getId();
                ob[1] = data.get(x).getId_announcement();
                ob[2] = data.get(x).getNama();
                ob[3] = data.get(x).getComments();
                tabelModel1.addRow(ob);
            }
        } catch (RemoteException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException ex) {
            Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblRespon = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtIdPen = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNamaR = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtKomentar = new javax.swing.JTextArea();
        btnTanggapi = new javax.swing.JButton();
        btnTambah = new javax.swing.JButton();
        btnTambahP = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtPP = new javax.swing.JTextField();
        txtJudul = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtIsi = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPengumuman = new javax.swing.JTable();
        btnLogout = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setText("Sistem Informasi Pengumuman");

        tblRespon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id", "Id Pengumuman", "Nama Responden", "Komentar"
            }
        ));
        tblRespon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblResponMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblRespon);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tanggapi Pengumuman"));

        jLabel2.setText("Id Pengumuman");

        jLabel3.setText("Nama Responden");

        jLabel4.setText("Komentar");

        txtKomentar.setColumns(20);
        txtKomentar.setRows(5);
        jScrollPane2.setViewportView(txtKomentar);

        btnTanggapi.setText("Tanggapi");
        btnTanggapi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTanggapiActionPerformed(evt);
            }
        });

        btnTambah.setText("Tambah");
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnTambah)
                                .addGap(190, 190, 190)
                                .addComponent(btnTanggapi))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtNamaR, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(txtIdPen, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtIdPen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNamaR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTanggapi)
                    .addComponent(btnTambah)))
        );

        btnTambahP.setText("Tambah Pengumuman");
        btnTambahP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahPActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Detail Pengumuman"));

        jLabel5.setText("Pembuat Pengumuman");

        jLabel6.setText("Judul Pengumuman");

        jLabel7.setText("Isi");

        txtPP.setEditable(false);

        txtJudul.setEditable(false);

        txtIsi.setEditable(false);
        txtIsi.setColumns(20);
        txtIsi.setRows(5);
        jScrollPane3.setViewportView(txtIsi);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtPP)
                            .addComponent(txtJudul, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtJudul, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 28, Short.MAX_VALUE))
        );

        tblPengumuman.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Id", "Nama", "Judul", "Isi"
            }
        ));
        tblPengumuman.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblPengumumanMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tblPengumuman);

        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnLogout)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(230, 230, 230)
                            .addComponent(jLabel1))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(21, 21, 21)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnTambahP, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGap(18, 18, 18)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 419, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(23, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(408, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(btnTambahP)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnLogout))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(56, 56, 56)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(318, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahPActionPerformed
        // TODO add your handling code here:
        TambahPengumuman tp = new TambahPengumuman();
        tp.setVisible(true);
        dispose();
        
    }//GEN-LAST:event_btnTambahPActionPerformed

    private void btnTanggapiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTanggapiActionPerformed
        // TODO add your handling code here:
        if (txtIdPen.getText().isEmpty() || txtNamaR.getText().isEmpty() || txtKomentar.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Lengkapi inputan pengumuman");
        } else {
            try {
                
                Comments cm = new Comments();
                cm.setId_announcement(txtIdPen.getText());
                cm.setNama(txtNamaR.getText());
                cm.setComments(txtKomentar.getText());

                Registry req = LocateRegistry.getRegistry("localhost", 1099);
                InstallInterface ii = (InstallInterface) req.lookup("server");

                ii.TambahComments(cm);
                bersihText();
                loadTable2();
                msr.tambahTanggapan();
            } catch (RemoteException ex) {
                Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NotBoundException ex) {
                Logger.getLogger(AdminLayoutMaster.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_btnTanggapiActionPerformed

    private void tblPengumumanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblPengumumanMouseClicked
        // TODO add your handling code here:
        int baris = tblPengumuman.getSelectedRow();
        txtPP.setText(tblPengumuman.getModel().getValueAt(baris, 1).toString());
        txtJudul.setText(tblPengumuman.getModel().getValueAt(baris, 2).toString());
        txtIsi.setText(tblPengumuman.getModel().getValueAt(baris, 3).toString());
        txtIdPen.setText(tblPengumuman.getModel().getValueAt(baris, 0).toString());
        
        loadTable2();
    }//GEN-LAST:event_tblPengumumanMouseClicked

    private void tblResponMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblResponMouseClicked
        // TODO add your handling code here:
        int baris = tblRespon.getSelectedRow();
        txtIdPen.setText(tblRespon.getModel().getValueAt(baris, 0).toString());
        txtNamaR.setText(tblRespon.getModel().getValueAt(baris, 1).toString());
        txtKomentar.setText(tblRespon.getModel().getValueAt(baris, 2).toString());
        
        btnTanggapi.setVisible(false);
        btnTambah.setVisible(true);
    }//GEN-LAST:event_tblResponMouseClicked

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
        btnTanggapi.setVisible(true);
        btnTambah.setVisible(false);
        txtIdPen.setText(null);
        txtNamaR.setText(null);
        txtKomentar.setText(null);
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        // TODO add your handling code here:
        Login lg = new Login();
        lg.setVisible(true);
        dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnTambah;
    private javax.swing.JButton btnTambahP;
    private javax.swing.JButton btnTanggapi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable tblPengumuman;
    private javax.swing.JTable tblRespon;
    private javax.swing.JTextField txtIdPen;
    private javax.swing.JTextArea txtIsi;
    private javax.swing.JTextField txtJudul;
    private javax.swing.JTextArea txtKomentar;
    private javax.swing.JTextField txtNamaR;
    private javax.swing.JTextField txtPP;
    // End of variables declaration//GEN-END:variables
}
