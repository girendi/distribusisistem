/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tgs.myinterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import org.tgs.mydao.Account;
import org.tgs.mydao.Announcement;
import org.tgs.mydao.Comments;

/**
 *
 * @author Gideon Panjaitan
 */
public interface InstallInterface extends Remote{
    public ArrayList findAllAccount() throws RemoteException;
    public ArrayList findAllAnnouncement() throws RemoteException;
    public ArrayList findAllComment() throws RemoteException;
    public ArrayList findComment(String idcom) throws RemoteException;
    public List Login(String nim,String pass)throws RemoteException;
    public int TambahAccount(Account a) throws RemoteException;
    public int updateAccount(Account a, String nim) throws RemoteException;
    public int deleteAccount(String nim) throws RemoteException;
    public int TambahAnnouncement(Announcement an) throws RemoteException;
    public int TambahComments(Comments cm) throws RemoteException;
}
