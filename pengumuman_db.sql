-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2017 at 04:26 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pengumuman_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `angkatan` varchar(4) NOT NULL,
  `role` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `nama`, `nim`, `password`, `prodi`, `angkatan`, `role`) VALUES
(1, 'Gideon panjaitan', '11415006', '11415006', 'D4TI', '2015', 'Admin'),
(3, 'Windalah', '11415008', '11415008', 'D4TI', '2015', 'User'),
(4, 'Hans Purba', '11415027', '11415027', 'D4TI', '2015', 'User'),
(5, 'Anas purba', '11415021', '11415021', 'D4TI', '2015', 'User'),
(8, 'Lily', '11415024', '11415024', 'D4TI', '2015', 'User'),
(9, 'Rinto', '11415004', '11415004', 'D4TI', '2015', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `nama`, `judul`, `isi`) VALUES
(1, 'Gideon', 'Kuliah Umum', 'Pada Hari Rabu kita akan mengadakan kuliah umum di audit'),
(2, 'Wawan', 'Persentasi Proyek', 'Pada Hari Kamis kita akan melakukan persentasi proyek pasti'),
(3, 'wqeqw', 'qwrqwr', 'qwrqwrqwrwqrwq'),
(4, 'Gideon', 'Perihal Pemberian Pengumuman', 'Dimohankan kepada seluruh pengguna agar memberikan pengumuman yang sepantasnya'),
(5, 'Gideon', 'Pengumuman', 'Selamat pagi');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) NOT NULL,
  `id_announcement` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `comment` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `id_announcement`, `nama`, `comment`) VALUES
(1, '1', 'Rinto', 'Kenapa harus rabu?'),
(2, '1', 'johan', 'Rabu kita persentasi pasti'),
(3, '2', 'Anas Cantik', 'Apah.........'),
(4, '3', 'Gideon', 'Apa-apa ini'),
(5, '4', 'Aren', 'Baik pak'),
(6, '4', 'Lestari', 'terima kasih');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nim` (`nim`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
